const webUrl = 'https://dev-central.zetta-system.com/account/login';

describe('Web Central', () => {


    const login = (username = 'ZT64003', password = 'pisi1234') => {
        cy.visit(webUrl);
        cy.get('input[name="username"]')
            .type(username);
        cy.get('input[name="password"]')
            .type(password);
        cy.intercept('POST', '**/authen-central/api/v1/authen/login-web').as('login1'); //ดักจับ request ที่เกิดขึ้นจากหน้าเว็บ
        cy.intercept('GET', '**/authen-central/api/v1/authen/me').as('login2');
        cy.intercept('GET', '**/authen-central/api/v1/menu/get').as('login3');
        cy.get("button:visible").contains("เข้าสู่ระบบ").click();
        cy.wait('@login1'); //รอให้ request นั้น ๆ ทำงานเสร็จ
        cy.wait('@login2');
        cy.wait('@login3');
        cy.url().should('include', 'main');
        cy.contains('เข้าสู่ระบบสำเร็จ');

    }

    const changePassword = (password, newPassword) => {
        cy.get('.ant-dropdown-link').click();
        cy.contains('เปลี่ยนรหัสผ่าน').click();
        cy.get('input[name="oldPassword"]')
            .type(password);
        cy.get('input[name="newPassword1"]')
            .type(newPassword);
        cy.get('input[name="newPassword2"]')
            .type(newPassword);
        cy.get("button:visible").contains('เปลี่ยนรหัสผ่าน')
            .click();
        cy.contains('เปลี่ยนรหัสผ่านสำเร็จ');

    }


    const updatePermission = (web, oldPer, newPer, result) => {
        cy.get('tbody').contains(web).parents('tr').within(() => {
            cy.get('td').eq(2).contains("แก้ไข").click({ force: true });
        })
        cy.get('.ant-modal-root').get('.ant-select-selection-item').contains(oldPer).click({ force: true });
        cy.get('.ant-modal-root').get('.ant-select-item-option-content').contains(newPer).click({ force: true });
        cy.contains("OK").click();
        cy.contains(result);

    }


    const searchUser = (userId = 'ZT64003') => {
        cy.intercept('POST', '**/authen-central/api/v1/menu/get-link').as('managePermission');
        cy.intercept('GET', '**/authen-central/api/v1/permission/get-users').as('get-users');
        cy.contains('Manage Permission').click();
        cy.wait('@managePermission');
        cy.wait('@get-users');
        cy.get('.ant-select-selector').eq(0).click();
        cy.get('.ant-select-selector').eq(0).type(userId).type('{enter}');
        cy.intercept('POST', '**/authen-central/api/v1/permission/get-permission').as('get-permissio');
        cy.get("button:visible").contains('ค้นหา')
            .click();
        cy.wait('@get-permissio')
    }


    it('Forget Password', () => {
        cy.visit(webUrl); //เข้าเว็บ
        cy.contains('ลืมรหัสผ่าน').click();
        cy.get('input[name="emailModal"]')
            .type('pisit.kae@dplusonline.net')
        cy.get("button:visible").contains('OK')
            .click()
        cy.contains('ทำรายการสำเร็จ : โปรดตรวจสอบ verify code ในเมลล์ของท่าน')

    })

    it('change password', () => {
        const password = 'pisi1234'
        const newPassword = '123456'
        login();
        changePassword(password, newPassword);
    })

    it('change password back', () => {
        const password = 'pisi1234'
        const newPassword = '123456'
        login('ZT64003', '123456');
        changePassword(newPassword, password);
    })

    it('reset password', () => {
        login();
        searchUser();
        cy.contains('รีเซ็ตรหัสผ่าน')
            .click();
        cy.contains('OK')
            .click();
        cy.contains('รีเซ็ตรหัสผ่านสำเร็จ');
    })
    it('test reset password', () => {
        login();
    })

    it('Update permission', () => {
        login();
        searchUser();
        updatePermission('Web Shipping', 'Shipping', 'Shipping', "มีข้อมูลซ้ำในระบบ กรุณาแก้ไขรายการก่อนสร้างสิทธิ์");
        updatePermission('Web Shipping', 'Shipping', 'Product', "แก้ไขสำเร็จ");
        updatePermission('Web Shipping', 'Product', 'Shipping', "แก้ไขสำเร็จ");
    })

    it('Add Permission', () => {
        login();
        searchUser();
        cy.contains('เพิ่มสิทธิ์').click();
        cy.intercept('GET', '**/authen-central/api/v1/permission/get-web-name').as('add');
        cy.wait('@add');
        cy.get('.ant-select-selector').contains('เลือก Web').click();
        cy.get('.ant-select-selector').contains('เลือก Web').type('IT Manage');
        cy.get('.ant-select-item-option-content').contains('IT Manage').click();
        cy.intercept('POST', '**/authen-central/api/v1/permission/create-permission').as('done');
        cy.contains('บันทึก').click();
        cy.wait('@done');
        cy.contains('เพิ่มสิทธิ์สำเร็จ');
    })

    it('Delete Permission', () => {
        login();
        searchUser();
        cy.get('tbody').contains('IT Manage').parents('tr').within(() => {
            cy.get('td').eq(2).contains("ลบ").click({ force: true });
        })
        cy.intercept('POST', '**/authen-central/api/v1/permission/delete-permission').as('delete');
        cy.contains("OK").click();
        cy.wait('@delete');
        cy.contains('ลบสิทธิ์ IT Manage ออกจาก ZT64003 สำเร็จ');
    })


})